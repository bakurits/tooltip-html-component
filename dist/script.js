(function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var a = typeof require == "function" && require;
        if (!u && a) return a(o, !0);
        if (i) return i(o, !0);
        throw new Error("Cannot find module '" + o + "'");
      }

      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function (e) {
        var n = t[o][1][e];
        return s(n ? n : e);
      }, f, f.exports, e, t, n, r);
    }

    return n[o].exports;
  }

  var i = typeof require == "function" && require;

  for (var o = 0; o < r.length; o++) s(r[o]);

  return s;
})({
  1: [function (require, module, exports) {
    class ToolTip extends HTMLElement {
      constructor() {
        super(); // Create a shadow root

        const shadow = this.attachShadow({
          mode: "open"
        });
        const wrapper = document.createElement("div");
        wrapper.setAttribute("class", "tooltip");
        const text = document.createElement("span");
        text.setAttribute("class", "tooltiptext");
        text.textContent = this.getAttribute("text");

        if (this.hasAttribute("direction")) {
          text.classList.add(this.getAttribute("direction"));
        }

        if (this.hasAttribute("color")) {
          text.setAttribute("style", "color:" + this.getAttribute("color"));
        }

        const style = document.createElement("style");
        style.textContent = `.tooltip {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black;
    }
    
    .tooltip .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
    
      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }
    
    .tooltip:hover .tooltiptext {
      visibility: visible;
    }
    .tooltiptext.right {
      top: -5px;
      left: 105%;
    }
    .tooltiptext.left {
      top: -5px;
      right: 105%;
    }
    .tooltiptext.down {
      width: 120px;
      top: 100%;
      left: 50%;
      margin-left: -60px;
    }
    .tooltiptext.up {
      width: 120px;
      bottom: 100%;
      left: 50%;
      margin-left: -60px;
    }
    `;
        console.log(this.parentElement.textContent);
        shadow.appendChild(style);
        shadow.appendChild(wrapper);
        wrapper.appendChild(text);
        wrapper.innerHTML += this.textContent;
        this.textContent = "";
      }

    }

    window.customElements.define("my-tooltip", ToolTip);
  }, {}]
}, {}, [1]);