class ToolTip extends HTMLElement {
  constructor() {
    super();
    // Create a shadow root
    const shadow = this.attachShadow({ mode: "open" });

    const wrapper = document.createElement("div");
    wrapper.setAttribute("class", "tooltip");

    const text = document.createElement("span");
    text.setAttribute("class", "tooltiptext");

    text.textContent = this.getAttribute("text");

    if (this.hasAttribute("direction")) {
      text.classList.add(this.getAttribute("direction"));
    }

    if (this.hasAttribute("color")) {
      text.setAttribute("style", "color:" + this.getAttribute("color"));
    }

    const style = document.createElement("style");
    style.textContent = `.tooltip {
      position: relative;
      display: inline-block;
      border-bottom: 1px dotted black;
    }
    
    .tooltip .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
    
      /* Position the tooltip */
      position: absolute;
      z-index: 1;
    }
    
    .tooltip:hover .tooltiptext {
      visibility: visible;
    }
    .tooltiptext.right {
      top: -5px;
      left: 105%;
    }
    .tooltiptext.left {
      top: -5px;
      right: 105%;
    }
    .tooltiptext.down {
      width: 120px;
      top: 100%;
      left: 50%;
      margin-left: -60px;
    }
    .tooltiptext.up {
      width: 120px;
      bottom: 100%;
      left: 50%;
      margin-left: -60px;
    }
    `;
    console.log(this.parentElement.textContent);

    shadow.appendChild(style);
    shadow.appendChild(wrapper);
    wrapper.appendChild(text);
    wrapper.innerHTML += this.textContent;
    this.textContent = "";
  }
}
window.customElements.define("my-tooltip", ToolTip);
